import javax.swing.JOptionPane;

public class CarreraTesji2 {
    
    
    
    public static void main (String[] args){
        byte opcion;
        byte cred = 0;
        byte taller;
        byte carrera;
        byte lugar;
        
        opcion = Byte.parseByte(JOptionPane.showInputDialog("¿Participaste en el desfile?\n1.-Si\n0.-No\nDigite su respuesta: "));
        
        if ( opcion == 1 ){
            
                cred = ( byte ) ( cred + 1 );
                JOptionPane.showMessageDialog(null,"Ganaste un credito, ¿En qué taller estas inscrito?: ");
                taller = Byte.parseByte(JOptionPane.showInputDialog("\n1.-Danza\n2.-Basquetbol\n3.-Futbol\n4.-TKD\n5.-Volieibol\nDigite su respuesta: "));
                       
                if ( taller == 1 ){
                            JOptionPane.showMessageDialog(null,"Uniforme escolar");
                }
                else if ( taller == 2 ){
                    JOptionPane.showMessageDialog(null,"Blanco");
                    }
                else if ( taller == 3 ){
                    JOptionPane.showMessageDialog(null,"Verde");
                }
                else if ( taller == 4 ) {
                    JOptionPane.showMessageDialog(null,"Uniforme TKD");
                }
                else if ( taller == 5 ){
                    JOptionPane.showMessageDialog(null,"Rojo");
                }
                else {
                    JOptionPane.showMessageDialog(null,"Opcion invalida");
                }                 
        }
        else {
            JOptionPane.showMessageDialog(null,"No tienes ningun credito");
            cred = 0;   
        }
        
        carrera = Byte.parseByte(JOptionPane.showInputDialog("¿Participaste en la carrera?\n1.-Si\n0.-No\nDigite su respuesta: "));
        
        if ( carrera == 1 ){
            cred = ( byte ) ( cred + 1 );
        }
        
        lugar = Byte.parseByte(JOptionPane.showInputDialog("¿Quedaste en uno de los 3 primeros lugares?\n1.-Si\n0.-No\nDigite su respuesta:"));
        
        if ( lugar == 1 ){
            cred = ( byte ) ( cred + 1 );
        }
        
        JOptionPane.showMessageDialog(null,"El total de creditos es "+ cred);
    
    }
}
