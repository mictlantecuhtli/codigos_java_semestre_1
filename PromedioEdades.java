import javax.swing.JOptionPane;

public class PromedioEdades {

    public static void main (String[] args){
        byte i;
        byte edad;
        byte sumaEdad = 0;
        float promedioEdades;

        JOptionPane.showMessageDialog(null,"Programa 5\n"
                + "Este programa, almacena la edad de 3 personas, posteriormente, calcula el "
                + "promedio de edad ");
        for ( i = 1; i <= 3; i++){
            edad = Byte.parseByte(JOptionPane.showInputDialog("Digite la edad del alumno " + i + ": "));
            sumaEdad = ( byte )( sumaEdad + edad );
        }
        promedioEdades = ( float )( sumaEdad / 3 );
        JOptionPane.showMessageDialog(null,"El promedio de edades es de: " + promedioEdades);
    }
}

