import javax.swing.JOptionPane;

public class ArregloBi {

    public static void main (String[] args) {

        //Declaración de variables
        byte calificaciones[][] = new byte[6][6];
        String materia[] = {"Cálculo Diferencial          ", "Fundamentos de programación  ", "Química                      ", "Fundamentos de investigación",
            "Matemáticas Discretas        ", "Desarrollo Sustentable       "};
        byte i;
        byte y;
        byte a;
        byte b;
        byte d;
        byte e;
        byte promedioFinal;
        short promedioUnidades = 0;
        
        JOptionPane.showMessageDialog(null,"Programa 10\n"
                + "Este programa almacena 5 calificaciones por materia, posteriormente"
                + "calcula el promedio por materia y el promedio final. Finalmente, imprime en forma"
                + "de tabla los resultados.");
        System.out.print("----------------------------------------------------------------------------------------------------------------------------------------\n");
        System.out.print("\t\t\t\tUnidad 1\t\tUnidad 2\t\tUnidad 3\t\tUnidad 4\t\tUnidad 5\t\tPromedio\n");
        System.out.print("----------------------------------------------------------------------------------------------------------------------------------------\n");
        for (i = 0; i < 6; i++) {
            short promedioMateria = 0;
            short sumaCalificaciones = 0;
            System.out.print(materia[i]);
            for (y = 0; y < 5; y++) {
                calificaciones[i][y] = Byte.parseByte(JOptionPane.showInputDialog("Ingresa la calificacion de la Unidad " + y + " de la materia " + materia[i]));
                sumaCalificaciones += calificaciones[i][y];
                System.out.print("\t|   " + calificaciones[i][y] + "\t       ");
            }
            promedioMateria = (short) (sumaCalificaciones / 5);
            promedioUnidades += (short) (promedioMateria);
            calificaciones[i][5]= (byte) (promedioMateria);
            System.out.print("|      " + calificaciones[i][5]);
            System.out.print("\n");
        }
        promedioFinal = (byte) (promedioUnidades / 6);
        System.out.print("----------------------------------------------------------------------------------------------------------------------------------------");
        System.out.print("\nEl promedio general es de: " + promedioFinal + "\n");
        System.out.print("----------------------------------------------------------------------------------------------------------------------------------------\n");
    }
}

