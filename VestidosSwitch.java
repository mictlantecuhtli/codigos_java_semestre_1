import javax.swing.JOptionPane;

public class VestidosSwitch {
    
    public static void main (String[] args){
        
        byte opcion;
        byte cantidad;
        short total = 0;

        JOptionPane.showMessageDialog(null,"Programa 4\n"
                + "Este programa te calculo el precio por la compra de vestidos de diferentes tallas");
        opcion = Byte.parseByte(JOptionPane.showInputDialog("Selecciona la talla\n1.-Talla chica\n2.-Talla mediana\n3.-Talla grande"));
    
        switch( opcion ){
            case 1:
                cantidad = Byte.parseByte(JOptionPane.showInputDialog("Haz seleccionado la talla chica (Talla 7)\n¿Cuántos vestidos compraste?: "));
                total = ( short ) ( cantidad * 300 );
            break;
            case 2:
                cantidad = Byte.parseByte(JOptionPane.showInputDialog("Haz seleccionado la talla mediana (Talla 10)\n¿Cuántos vestidos compraste?: "));
                total = ( short ) ( cantidad * 400 );
            break;
            case 3:
                cantidad = Byte.parseByte(JOptionPane.showInputDialog("Haz seleccionado la talla grande (Talla 16)\n¿Cuántos vestidos compraste?: "));
                total = ( short ) ( cantidad * 500 );
            break;
            default:
                JOptionPane.showMessageDialog(null,"Opción inválida");
            break;
        }
        JOptionPane.showMessageDialog(null,"el total a pagar es: " + total);
    }
}
