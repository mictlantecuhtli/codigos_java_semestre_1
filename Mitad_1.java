import javax.swing.JOptionPane;

public class Mitad_1 {
    
    public static void main (String[] args){
        short numero;
        short doble;
        float tercera;
        float mitad;
        
        JOptionPane.showMessageDialog(null,"Programa 1\n"
                + "Este programa te calcula la mitad de la tercera parte de un número");
        numero = Byte.parseByte(JOptionPane.showInputDialog("Digite un numero (0-200): "));

        if ( ( numero > 0 ) && ( numero <= 200 ) ){
            doble = ( short ) ( numero * 2 );
            tercera = ( float ) ( doble / 3 );
            mitad = ( float ) ( tercera / 2 );
            JOptionPane.showMessageDialog(null,"La mitad de la tercera parte de " + numero + " es: " + mitad);
        }
        else {
            JOptionPane.showMessageDialog(null,"Numero invalido");
        }
    }
}


