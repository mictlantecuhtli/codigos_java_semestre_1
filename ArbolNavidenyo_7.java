import java.util.Scanner;
import javax.swing.JOptionPane;

public class ArbolNavidenyo_7 {

    public static void main (String[] args) {
        byte i;
        byte y;
        byte x;
        byte j;
        Scanner teclado = new Scanner(System.in);

        JOptionPane.showMessageDialog(null,"Programa 7\n"
                + "Este programa te dibujo un arbol de navidad");
        for ( i = 1; i <= 8; i++){
            for ( y = 1; y <= 8-i; y++){
                System.out.print(" ");
            }
            for ( x = 1; x <= ( i * 2 ) - 1; x++){
                System.out.print("*");
            }
            System.out.print("\n");
        }
        for ( j = 1; j <= 4; j++) {
            System.out.print("      * * \n");
        }
    }
}

