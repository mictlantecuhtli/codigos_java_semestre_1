import javax.swing.JOptionPane;

public class Factorial_6 {

    public static void main (String[] args){
        int factorial;
        int i;
        int z = 1;
        int resFac;

        JOptionPane.showMessageDialog(null,"Programa 6\n"
                + "Este programa te solicita un número, despues, calcula el factorial de dicho digito");
        factorial = Byte.parseByte(JOptionPane.showInputDialog("¿Qué número desea ver su factorial?: "));

        for ( i = 1; i <= factorial; i++ ){

            resFac = ( int )( z * i );
            JOptionPane.showMessageDialog(null, z + " * " + i + " = " + resFac + "\n");
            z = resFac;
        }
        JOptionPane.showMessageDialog(null,"El factorial de " + factorial + " es: " + z);
    }
}
