import javax.swing.JOptionPane;

public class CafeteriaTesji {

    public static void main (String[] args){
        byte color;
        byte carrera;
        short totalPagar = 0;
        float descuento = 0;
        float totalPagarDescuento = 0;

        JOptionPane.showMessageDialog(null,"Programa 3\n"
                + "Este programa te hace un descuento en tu compra, con base al color de la pelota que saques");
        JOptionPane.showMessageDialog(null,"¿De qué color es la pelota que acabas de sacar?");
        JOptionPane.showMessageDialog(null,"\n1.-Verde\n2.-Amarilla\n3.-Roja");
        color = Byte.parseByte(JOptionPane.showInputDialog("\nDigite su respuesta: "));

        if ( color == 1 ){
            JOptionPane.showMessageDialog(null,"Felicidades, tienes un descuento del 10%");
            totalPagar = Byte.parseByte(JOptionPane.showInputDialog("\n¿Cuál es tu monto a pagar?: "));
            descuento = ( float ) ( totalPagar * 0.10 );
            totalPagarDescuento = ( float )( totalPagar - descuento );
        }
        else if ( color == 2 ) {
            JOptionPane.showMessageDialog(null,"Felicidades, tienes un descuento del 5%");
            totalPagar = Byte.parseByte(JOptionPane.showInputDialog("\n¿Cuál es tu monto a pagar?: "));
            descuento = ( float ) ( totalPagar * 0.05 );
            totalPagarDescuento = ( float )( totalPagar - descuento );
        }
        else if ( color == 3 ) {
            JOptionPane.showMessageDialog(null,"Felicidades, tienes un descuento del 15%");
            totalPagar = Byte.parseByte(JOptionPane.showInputDialog("\n¿Cuál es tu monto a pagar?: "));
            descuento = ( float ) ( totalPagar * 0.15 );
            totalPagarDescuento = ( float )( totalPagar - descuento );
        }
        else {
            JOptionPane.showMessageDialog(null,"\nNúmero inválido");
        }
        JOptionPane.showMessageDialog(null,"\nEl total a pagar: " + totalPagarDescuento + " pesos");
    }
}

