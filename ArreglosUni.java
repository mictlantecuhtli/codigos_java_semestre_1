import javax.swing.JOptionPane;

public class ArreglosUni {
    public static void main (String[] args){

        //Declaración de variables
        byte arre1 [] = new byte [5];
        String titulos [ ]= {"Uni 1","Uni 2","Uni 3","Uni 4","Uni 5"};
        byte i;
        byte y;
        byte a;
        float promedio = 0;
        float promedioFinal;

        JOptionPane.showMessageDialog(null,"Programa 7\n"
                + "Este programa almacena en un arreglo la calificación de 5 unidades y calcula el "
                + "promedio general");

        for ( i = 0; i < 5; i++ ){
            arre1[i] = Byte.parseByte(JOptionPane.showInputDialog("Ingrese la calificación de la " + titulos[i]));
            promedio += arre1[i];
        }
        promedioFinal = promedio / 5;
        for ( y = 0; y < 5; y++ ){
            System.out.print(titulos[y] + "\t");
        }
        System.out.print("\n");
        for ( a = 0; a < 5; a++ ) {
            System.out.print(arre1[a] + "\t");
        }
        System.out.print("\nEl promedio es: " + promedioFinal);
    }
}
